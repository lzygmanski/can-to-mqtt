const canProvider = require('../providers/canProvider');
const canLogger = require('../utils/can/canLogger');
const getSchemaFromCan = require('../utils/can/getSchemaFromCan');
const relay = require('../types/relay'); 

const canToMqtt = () => {
    console.info('CAN To MQTT converter started!');
    canProvider.on('readable', () => {
        let data;

        while (null !== (data = canProvider.read(15))) {
            canLogger(data, false, 'String')

            const schema = getSchemaFromCan(data);
            
            if(!schema) return;

            const { frameSchema } = schema;

            switch(frameSchema.code) {
                case 'relay':
                    relay.callMqtt(data, schema);
                    break;
                default:
                    break;
            }
        }
    });
};

module.exports = canToMqtt;
