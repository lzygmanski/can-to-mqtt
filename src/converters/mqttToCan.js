const mqttProvider = require('../providers/mqttProvider');
const getSchemaFromMqtt = require('../utils/mqtt/getSchemaFromMqtt');
const mqttLogger = require('../utils/mqtt/mqttLogger');
const relay = require('../types/relay'); 

const mqttToCan = () => {
    mqttProvider.on('message', function (topic, payload) {
        mqttLogger(topic, payload, false);

        const schema = getSchemaFromMqtt(topic);
            
        if(!schema) return;

        const { frameSchema } = schema;

        switch(frameSchema.code) {
            case 'relay':
                relay.callCan(topic, payload, schema);
                break;
            default:
                break;
        }
    })
};

module.exports = mqttToCan;