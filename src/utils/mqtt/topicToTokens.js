const topicToTokens = (topic) => topic.split('/');

module.exports = topicToTokens;
