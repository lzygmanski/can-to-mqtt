const _ = require('lodash');
const colors = require('../../utils/colors');
const getSchema = require('../../utils/getSchema');
const topicToObject = require('./topicToObject');

let formatter = data => data; 

const canLogger = (topic, payload, all, type) => {
    try {
        if (type) {
            formatter = require(`./topicTo${type}`);
        };

        const { frameType } = topicToObject(topic);
        const includeInSchema = _.find(getSchema().frameTypes, { code: frameType });

        if(includeInSchema || all) {
            console.info(colors.fgCyan, {
                topic: formatter(topic),
                payload: payload.toString(),
            }, colors.reset);
        }
    }
      catch(err) {
        console.error(colors.fgRed, `Type ${type} is not supported.`, colors.reset);
        if (err) console.error(colors.fgRed, err, colors.reset);
    }
}

module.exports = canLogger;
