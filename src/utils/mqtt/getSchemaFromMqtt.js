const getBaseSchema = require('../getBaseSchema');
const topicToObject = require('./topicToObject');

const getSchemaFromMqtt = topic => getBaseSchema(topic, 'code', topicToObject);

module.exports = getSchemaFromMqtt;
