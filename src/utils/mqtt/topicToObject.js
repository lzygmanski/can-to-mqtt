const topicToTokens = require('./topicToTokens');

const topicToObject = topic => {
    const tokens = topicToTokens(topic);
    return ({
        frameType: tokens[0],
        group: tokens[1],
        module: tokens[2]
    })
};

module.exports = topicToObject;
