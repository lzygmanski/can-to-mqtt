const _ = require('lodash');
const schema = require('../../schema');
const config = require('../../config');

const customizer = (objValue, srcValue) => {
    if (_.isArray(objValue)) {
      return objValue.concat(srcValue);
    }
  }

const getSchema = () => _.mergeWith(schema, config.schema, customizer);

module.exports = getSchema;