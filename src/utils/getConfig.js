const _ = require('lodash');
const config = require('../../config');

let env;
try {
    env = require('../../env');
} catch (e) {
    console.log('No env.json provided')
}

const envConfig = key => env ? _.get(env, key) : undefined;
const options = key => _.get(config.options, key);

const getConfig = (key) => envConfig(key) || options(key);

module.exports = getConfig;