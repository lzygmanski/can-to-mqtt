const _ = require('lodash');
const colors = require('../../utils/colors');
const getSchema = require('../../utils/getSchema');
const bufferToObject = require('./bufferToObject');

const canLogger = (data, all, type = 'Object') => {
    try {
        const formatter = require(`./bufferTo${type}`);
        const { frameType } = bufferToObject(data);
        const includeInSchema = _.find(getSchema().frameTypes, { id: frameType });

        if(includeInSchema || all) {
            console.info(colors.fgMagenta, formatter(data), colors.reset);
        }
    }
      catch(err) {
        console.error(colors.fgRed, `Type ${type} is not supported.`, colors.reset);
        if (err) console.error(colors.fgRed, err, colors.reset);
    }
}

module.exports = canLogger;
