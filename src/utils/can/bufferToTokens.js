const bufferToString = require('./bufferToString');

const bufferToTokens = (data) => bufferToString(data).match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g);

module.exports = bufferToTokens;
