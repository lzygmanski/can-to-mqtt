const bufferToString = (data, type = 'hex') => Buffer.isBuffer(data) ? data.toString(type) : data;

module.exports = bufferToString;
