const bufferToTokens = require('./bufferToTokens');

const bufferToObject = (data) => {
    const tokens = bufferToTokens(data);
    return ({
        start: tokens[0],
        frameType: tokens[1] + tokens[2][0],
        flag: tokens[2][1],
        module: tokens[3],
        group: tokens[4],
        chksum: tokens[13],
        stop: tokens[14]
    })
};

module.exports = bufferToObject;
