const getSchema = require('../getSchema');
const getConfig = require('../getConfig');
const bufferToTokens = require('./bufferToTokens');

const appGroup = getConfig('can.group');
const appModule = getConfig('can.module');

const generateChksum = (msg) => { 
    const sum = bufferToTokens(msg).reduce((previousValue, currentValue) => (
        previousValue + parseInt(currentValue, 16)
    ), 0);

    return sum.toString(16).substr(-2)
}


const createMesseage = (prefix, postfix) => {
    const data = prefix + appModule + appGroup + postfix;
    const stringMsg = getSchema().messeage.start + data + generateChksum(data) + getSchema().messeage.stop;
    return Buffer.from(stringMsg, 'hex');
}

module.exports = createMesseage;