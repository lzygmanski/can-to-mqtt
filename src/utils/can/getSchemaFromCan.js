const getBaseSchema = require('../getBaseSchema');
const bufferToObject = require('./bufferToObject');

const getSchemaFromCan = data => getBaseSchema(data, 'id', bufferToObject);

module.exports = getSchemaFromCan;
