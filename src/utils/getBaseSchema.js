const _ = require('lodash');
const getSchema = require('../utils/getSchema');

const getBaseSchema = (data, key, mapper) => {
    const { frameType, group, module: canModule } = mapper(data);

    const schema = getSchema();
    const frameSchema = _.find(schema.frameTypes, { [key]: frameType });
    const groupSchema = _.find(schema.groups, { [key]: group });

    if(!frameSchema && !groupSchema) return;

    const moduleSchema = _.find(groupSchema.modules, { [key]: canModule });

    if(!moduleSchema) return;

    return ({
        frameSchema,
        groupSchema,
        moduleSchema,
    })
}

module.exports = getBaseSchema;
