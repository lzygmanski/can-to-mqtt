const net = require('net');
const getConfig = require('../utils/getConfig');

const port = getConfig('can.port');
const host = getConfig('can.host');

const canProvider = net.createConnection(port, host, () => {
  console.info('Connected to CAN getaway!');
});

canProvider.on('error', err => {
    console.error('CAN Error!');
    if (err) console.error(err);
});

module.exports = canProvider;
