const mqtt = require('mqtt');
const getConfig = require('../utils/getConfig');

const port = getConfig('mqtt.port');
const host = getConfig('mqtt.host');
const username = getConfig('mqtt.username');
const password = getConfig('mqtt.password');
const clientId = getConfig('mqtt.clientId');


const mqttProvider  = mqtt.connect(`mqtt://${host}`, {
    port,
    clientId,
    username,
    password,
});
 
mqttProvider.on('connect', () => {
    console.info('Connected to MQTT broker!');
});

mqttProvider.subscribe("#");

mqttProvider.on('error', err => {
    console.error('MQTT Error!');
    if (err) console.error(err);
});

module.exports = mqttProvider;
