const _ = require('lodash');
const createMesseage = require('../utils/can/createMesseage')
const getSchema = require('../utils/getSchema');
const mqttProvider = require('../providers/mqttProvider');
const canProvider = require('../providers/canProvider');
const bufferToTokens = require('../utils/can/bufferToTokens');
const topicToTokens = require('../utils/mqtt/topicToTokens');

const callMqtt = (data, schema) => {
    const tokens = bufferToTokens(data);
    const { frameSchema, groupSchema, moduleSchema } = schema;

    const chanelSchema = _.find(moduleSchema.chanels, { id: tokens[7] });

    if(!chanelSchema) return;
    
    mqttProvider.publish(
        `${frameSchema.code}/${groupSchema.code}/${moduleSchema.code}/${chanelSchema.code}`,
        tokens[8],
        { retain: true }
    )
}

const callCan = (topic, payload, schema) => {
    // Examples
    //
    // Turn off 1:          AA 10 A0 F0 F0 00 01 06 01 00 FF FF FF 95 A5 
    // Trun off 2:          AA 10 A0 F0 F0 00 02 06 01 00 FF FF FF 96 A5
    // Turn off All:        AA 10 A0 F0 F0 00 03 06 01 00 FF FF FF 97 A5
    //
    // Turn on 1:           AA 10 A0 F0 F0 01 01 06 01 00 FF FF FF 96 A5 
    // Turn on 2:           AA 10 A0 F0 F0 01 02 06 01 00 FF FF FF 97 A5
    // Turn on All:         AA 10 A0 F0 F0 01 03 06 01 00 FF FF FF 98 A5
    //
    // Toggle 1:            AA 10 A0 F0 F0 02 01 06 01 00 FF FF FF 97 A5
    // Toggle 2:            AA 10 A0 F0 F0 02 02 06 01 00 FF FF FF 98 A5 
    // Toggle All:          AA 10 A0 F0 F0 02 03 06 01 00 FF FF FF 99 A5
    // Exmaples end

    const tokens = topicToTokens(topic);

    if (!tokens[4]) return;

    const frameId = _.find(getSchema().frameTypes, { code: 'control' }).id;
    const flag = '0';

    const { frameSchema, groupSchema, moduleSchema } = schema;
    const chanelSchema = _.find(moduleSchema.chanels, { code: tokens[3] });
    
    if(!chanelSchema) return;

    const prefix = frameId + flag;
    const postfix = payload.toString() + chanelSchema.control + moduleSchema.id + groupSchema.id + '00ffffff';

    const data = createMesseage(prefix, postfix);
    canProvider.write(data);

}

module.exports = { callMqtt, callCan };
